package com.wissen.wcart.config.server.constant;

import com.wissen.wcart.common.constant.GlobalConstants;

public interface ServiceConstants extends GlobalConstants {

	public static final String SERVICE_NAME = "WCart-Config-Server";

}
