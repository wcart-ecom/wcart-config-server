package com.wissen.wcart.config.server.aop;

import static com.wissen.wcart.config.server.constant.ServiceConstants.*;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import com.wissen.wcart.common.util.LogUtil;

@Aspect
@Component
public class LoggingAspect {

	private static final String ENTERING = "Entering Method";
	private static final String EXITING = "Exiting Method";
	private static final String BODY = "Body" + SPACE + DOUBLE_COLON + SPACE;

	@Before("execution(* com.wissen.wcart.config.server..*(..)))")
	public void logMethodEntering(JoinPoint joinPoint) {
		Signature signature = joinPoint.getSignature();

		String className = signature.getDeclaringType().getSimpleName();
		String methodName = signature.getName();

		LogUtil.info(SERVICE_NAME, className, methodName, ENTERING);

		for (Object obj : joinPoint.getArgs()) {
			LogUtil.info(SERVICE_NAME, className, methodName, BODY + obj.toString());
		}

	}

	@Before("execution(* com.wissen.wcart.config.server..*(..)))")
	public void logMethodExiting(JoinPoint joinPoint) {
		Signature signature = joinPoint.getSignature();

		String className = signature.getDeclaringType().getSimpleName();
		String methodName = signature.getName();

		LogUtil.info(SERVICE_NAME, className, methodName, EXITING);

	}

}
